
// Functions
    //Parameters and Arguments

//  function printName() {
//     let nickname = prompt("Enter your nickname: ");
//     console.log("Hi, " + nickname);
//  }   

//     printName();

function printName(name) {
    // let nickname = prompt("Enter your nickname: ");
    console.log("Hi, " + name);
 }   

    printName("Cee");


    let sampleVariable = "Cardo";

    printName(sampleVariable);




    function checkDivisibilityBy8(num) {
        let remainder = num % 8;
        console.log("The remainder of " + num + " divided by 8: " + remainder);
        let isDivisibleBy8 = remainder == 0;
        console.log("Is " + num + " divisible by 8?");
        console.log(isDivisibleBy8);

    }

    checkDivisibilityBy8(64);
    checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);

	// Mini-activity

	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder)
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?")
		console.log(isDivisibleBy4);

	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);



	// Functions as arguments 
	// Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	// Adding and removing the parentheses "()" impacts the output of JS heavily 
	// When a function is used with parenthese "()", it denotes invoking/calling a function
	// A function used without a parenthesis is normally associated with using the function as an argument to another function.

	invokeFunction(argumentFunction);

	// using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	}

	createFullName('Juan', 'Dela', 'Cruz');
	createFullName('Cruz', 'Dela', 'Juan');
	createFullName('Juan', 'Dela');
	createFullName('Juan', 'Dela', 'Cruz', 'III');

	// using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

    /***************************************/

    function printFriends(friend1, friend2, friend3) {
        console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3 + ".");

    }

    printFriends("a", "b", "c");

    //Return Statement

    function returnFullName(firstName,middleName,lastName) {
        return firstName + ' ' + middleName + ' ' + lastName;
        console.log("This will not be printed!");
    }

    let completeName1 = returnFullName("Monkey", "D", "Luffy");
    let completeName2 = returnFullName("Cardo", "Tanggol", "Dalisay");

    console.log(completeName1 + " is my bestfriend!");
    console.log(completeName2 + " is my friend!");


    function getSquareArea(side) {
        return side **2;
    }

    let areaSquare = getSquareArea(4);

    console.log("The result of getting the area of a square with length of 4: ");
    console.log(areaSquare); //16

    function computeSum(num1, num2, num3) {
        return num1 + num2 + num3;
    }

    let sumOfThreeDigits = computeSum(1, 2, 3);
    console.log("The sum of three numbers are: ");
    console.log(sumOfThreeDigits); //6


    
    function compareToOneHundred(num) {
        return num === 100;
    }

    let booleanValue = compareToOneHundred(99);
    console.log("Is this one hundred?");
    console.log(booleanValue);
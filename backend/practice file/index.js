function test (a, b) {
    const total = a + b;
    console.log("output: " + total);
}

test(5, 5);

/*******************************************************/

//using console.log

function calculateSum(a, b) {
    const sum = a + b;
    console.log('The sum is:', sum);
  }
  
  calculateSum(2, 3); // Output: The sum is: 5


//using return statement

function calculate(a, b) {
    const sum2 = a + b;
    return sum2;
  }
  
  const result = calculate(3, 3);
 console.log('The sum is:', result); // Output: The sum is: 6
  
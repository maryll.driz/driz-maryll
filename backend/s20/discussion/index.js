console.log("Hello World");

//Arithmetic Operators
// + - * / % (modulo operator)

    let x = 1397;
    let y = 7831;

    let product = x * y; // ** is the exponentiation in js - power of the right operand
    console.log("Result of product operator: " + product);

    let difference = x - y;
    console.log("Result of difference operator: " + difference);

    let quotient = x / y;
    console.log("Result of quotient operator: " + quotient);

    let remainder = 15 % 4;
    console.log("Result of remainder operator: " + remainder);

//Assignment Operator
//Basic Assignment operator (=)

let assignmentNumber = 8;

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
assignmentNumber +=2;
console.log(assignmentNumber);

//Subtraction/Multiplication/Division Assignment operator

assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of *= : " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of /= : " + assignmentNumber);

//Multiple Operators and Parentheses

/** 
 *  1. 3 * 4 = 12
 *  2. 12 / 5 = 2.4
 *  3. 1 + 2 = 3
 *  4. 3 - 2.4 = 0.6
 *
 * **/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

// 1. 4/5 = 0.8
// 2. 2 - 3 = -1
// 3. -1 * 0.8 = -0.8
// 4. 1 + -0.8 = 0.2

let pemdas = 1 + (2 - 3) * (4/5);
console.log(pemdas);

//Increment and Decrement
//Operators that add or subtract values by 1 and reassigns the value
// of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z;
//the value of "z" is added by a value of one before returning the
//value and storing it in the variable "increment"
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//re-assign then increase
increment = z++;
//he value of z is returned and stored in the variable "increment"
//then the value of z is increased by one
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);


let decrement = --z;
console.log("Result of pre-increment: " + decrement);
console.log("Result of pre-increment: " + z);

decrement = z--;
console.log("Result of post-increment: " + decrement);
console.log("Result of post-increment: " + z);

//type coercion
// type coercion is the automatic or implicit conversion of values
// from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //"1012"
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//the boolean "true" is associated with the value of 1

let numE = true + 1;
console.log(numE);

//the boolean "false" is associated with the value of 0

let numF = false + 1;
console.log(numF);

//Comparison Operators

let juan = 'juan';

//Equality Operator

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);

//Inequality Operator (!=)

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);

//Strict Equality Operator (===)

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);

//Strict Inequality Operator (!==)

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);

//Relational Operators

let abc = 50;
let def = 65;

let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";
console.log(abc > numStr);
console.log(def <= numStr);

let str = "twenty";
console.log(def >= str);
//Syntax, Statements, and comments

// console.log("We tell the computer to log this in the console.");
// alert("We tell the computer to display an alert with this message!")

//Statements - in programming are instructions that we tell the computer to perform
//JS Statements usually end with semicolon (;)

//Syntax - it is the set of rules that describes how statements must be constructed

//Comments
//for us to create a comment, we use CTRL + /
//We have single line and multi-line comments

// console.log("Hello!");

// alert("This is an alert!");
// alert("This is another alert");

//whitespaces (spaces and line breaks) can impact functionality in many computer languages,
//but not in JS.

//Variables
//This is used to contain data

//Declare variables
    //tell our devices that a variableName is created and is ready to store data
    //Syntax
        //let or const variableName;

let myVariable;
    console.log(myVariable); //undefined

    //Variables must be declared first before they are used
    //using variables before they are declared will return an error

let hello;
    console.log(hello);

    //declaring and initializing variables
    //Syntax
        //let or const variableName = initialValue;


    let productName = 'desktop computer';
    console.log(productName);
    // console.log(`The ${productName} has been ordered.`);

    let productPrice = 18999;
    console.log(productPrice);

    //let - variables that can be changed
    //const - variables that cannot be changed

    const interest = 3.539;

    //re-assigning variable values

    productName = 'Laptop';
    console.log(productName);

    // interest = 3.5;
    // console.log(interest);

    //we cannot replace constant values

    //let variable cannot be re-declared within its scope

    let friend = 'Kate';
     friend = 'Jane';

     //Declare a variable
     let supplier;

    //Initialization is done after the variable has been declared
     supplier = "John Smith Tradings";

     supplier = "Zuitt Store";

    //we cannot declare a const variable without initialization
    //  const pi = 3.1416;
    //  console.log(pi);

    //Multiple variable declarations
    // let productCode = 'DC017';
    // const productBrand = 'Dell';

    //array
    let productCode = 'DC017', productBrand = 'Dell';
    console.log(productCode, productBrand);

    //Data Types

    //Strings

    let country = 'Philippines';
    let province = "Metro Manila";

    //Concatenating Strings

    let fullAddress = province + ', ' + country;
    console.log(fullAddress);

    let greeting = 'I live in the ' + country;
    console.log(greeting);

    //the escape character (\) in strings in combination with other characters can produce different
    //effects

    //"\n" refers to creating a new line in between text

    let mailAddress = 'Metro Manila \n\nPhilippines';
    console.log(mailAddress);

    let message = "John's employees went home early";
    console.log(message);

    message = 'John\'s employees went home early';
    console.log(message);

    //Numbers

    let headcount = 26;
    console.log(headcount);
    let grade = 98.7;
    console.log(grade);
    let planetDistance = 2e10;
    console.log(planetDistance);

    //Combine text and strings
    console.log("John's first grade last quarter is " + grade);
    

    //Boolean
    //2 values - true & false

    let isMarried = false;
    let isGoodConduct = true;
    console.log(isMarried);

    console.log("isGoodConduct: " + isGoodConduct);

    //Arrays

    // Syntax
        //let/const arrayName = [elementA, elementB, ...]

        let grades = [98.7, 92.1, 90.7, 98.6];
        console.log(grades);

        //Objects

        let myGrades = {
            firstGrading: 98.7,
            secondGrading: 92.1,
            thirdGrading: 90.7,
            fourthGrading: 94.6
        }
        console.log(myGrades);

        let person = {
            fullName: 'Juan Dela Cruz',
            age: 35,
            isMarried: false,
            contact: ["+639123456789", "87000"],
            address: {
                houseNumber: '345',
                city: 'Manila'
            }
        }
    console.log(person);

    //type of operator

    console.log(typeof person); //object
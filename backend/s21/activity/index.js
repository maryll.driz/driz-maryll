/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

//answer:
    function getUserInfo() {
        console.log("getUserInfo();");

        const userInfo = {
            name : "Maryll",
            age : 24,
            address : "Calapan Oriental Mindoro",
            isMarried : false,
            petName : "pepper"

        }

        return userInfo;
}
    const userInfo = getUserInfo();
    console.log(userInfo);


/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

//answer:
    function getArtistsArray() {
        console.log("getArtistsArray();");

        const artistsArr = ["Moira", "Vivoree", "Sarah G.", "Ben & Ben","Daryl"];
        return artistsArr;
    }

    const artistsArr = getArtistsArray();
    console.log(artistsArr);
    



/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

//answer:
function getSongsArray() {
    console.log("getSongsArray();");

    const songsArr = ["September Ends", "Cupid", "", "Dandelions","Uhaw"];
    return songsArr;
}

const songsArr = getSongsArray();
console.log(songsArr);


/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

//Answer:
    function getMoviesArray() {
        console.log("getMoviesArray();");

        let moviesArr = ["Wednesday", "3 idiots", "big bang theory", "dynasty", "kdrama"];
        return moviesArr;

    }

    let moviesArr = getMoviesArray();
    console.log(moviesArr);


/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/

//Answer
    function getPrimeNumberArray() {
        console.log("getPrimeNumberArray();");

        let primeNumbersArr = ["2", "3", "5", "7", "17"];
        return primeNumbersArr;

    }
    let primeNumbersArr = getPrimeNumberArray();
    console.log(primeNumbersArr);



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
        getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
        getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
        getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
        getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

    }
} catch(err){


}
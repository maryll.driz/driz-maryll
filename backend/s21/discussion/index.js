/*
let isLegalAge = true;
let isRegistered = false;

// && (AND), || (OR), ! (NOT)

//&& 
//Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of AND operator: " + allRequirementsMet);

//||
//Returns true if ONE of the operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of OR operator: " + someRequirementsMet); //true

//!
//Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT operator: " + someRequirementsNotMet); 
*/

//Mini-Activity - log your favorite line 20 times in the console.

// for (let i = 1; i < 21; i++) {
//     console.log (i + " reading & learning");
//   }

//   function printLine() {
//     console.log("'Carpe diem. Seize the day, boys. Make your lives extraordinary.'");
//   }

//   printLine();

  //Functions
  //lines/blocks of code that tell our devices to perform a certain task when called/invoked

  //Function declaration

  /* 
    Syntax:

    function functionName() {
        code block (statement)
    }

  */
    //Function Declaration
    function printName() {
        //code block (statement)
        console.log("My name is Maryll.");
    }

    //Function Invocation
    printName();

    declaredFunction();
    //Function declaration vs expressions

    //Function Declaration
        //function declaration is created with the function keyword and adding a function name
    //they are "saved for later use"


    function declaredFunction() {
        console.log("Hello from declaredFunction!");
    }

    declaredFunction();

    //Function Expression
        //function expression is stored in a variable
        //function expression is an anonymous function assigned to the variable function

    // variableFunction();

    let variableFunction = function() {
        console.log("Hello from function expression!");
    }

    variableFunction();

    //a function expression of function names funcName assigned to the variable funcExpression

    let funcExpression = function funcName() {
        console.log("Hello from the other side!");
    }

    funcExpression();

    //We can also reassigned declared functions and function expressions to new anonymous functions

    declaredFunction = function() {
        console.log("updated declaredFunction");
    }

    declaredFunction();

    funcExpression = function() {
        console.log("updated funcExpression");
    }

    funcExpression();
    
    const constantFunc = function() {
        console.log("Initialized with const!");
    }

    constantFunc();

    // constantFunc = function() {
    //     console.log("Cannot be reassigned!");
    // }

    // constantFunc();

    //Function Scoping
    /*
        Scope - accessibility/visibility of variables
        JS Variables ha s3 types of scope:
        1. local/block scope
        2. global scope
        3. function scope

    */

        // {
        //     let a = 1;
        // }

        // let a = 1;

        // function sample(){
        //     let a = 1;
        // }

        {
            let localVar = "Armando Perez";
        }

        let globalVar = "Mr. Worldwide";

        // console.log(localVar);

        console.log(globalVar);

        function showNames() {

            var functionVar = "Joe";
            const functionConst = "John";
            let functionLet = "Jane";

            console.log(functionVar);
            console.log(functionConst);
            console.log(functionLet);
        }

        // console.log(functionVar);
        // console.log(functionConst);
        // console.log(functionLet);

        // console.log(showNames());

        showNames();

        //Nested Functions

        function myNewFunction() {
            let name = "Jane";

            function nestedFunction() {
                let nestedName = "John";
                console.log(nestedName);
            }

             console.log(name);

            nestedFunction();
        }

        myNewFunction(); 
        //nestedFunction(); //is declared inside the myNewFunction scope

        //FUNCTION AND GLOBAL SCOPR VARIABLE

        //Global Scoped Variable

        let globalName = "Cardo";

        function myNewFunction2() {
            let nameInside = "Hillary";
            console.log(globalName);
        }

        myNewFunction2();

        function showSampleAlert() {
            alert("Hello, Earthlings! This is from a function!");

        }

        // showSampleAlert(); // using prompt()

        console.log("I will only log in the console when the alert is dismissed");

    // let samplePrompt = prompt("Enter your Name: ");
    // console.log("hi, I am " + samplePrompt);
        
    //prompt returns an empty string when there is no input or null if the user cancels the prompt()

    function printWelcomeMessage() {
        let firstName = prompt("Enter your first name: ");
        let lastName = prompt("Enter your last name: ");

        console.log("Hello, " + firstName + " " + lastName + "!");
        console.log("Welcome to my page!");
    }

    // printWelcomeMessage();

    //Return Statement

    /*
        The return statement allows us to output a value from a function to be passed to the line/block
        of code that invoked/called the function

    */

        function returnFullName() {
            return "Jeffrey" + ' ' + "Smith" + ' ' + "Bezos";
            console.log("This message will not be printed!");
        }

        let fullName = returnFullName();
        console.log(fullName);

        function returnFullAddress() {
            let fullAddress = {
                street : "#44 Maharlika St.",
                city : "Cainta",
                province : "Rizal"
            }

            return fullAddress;
        }

        let myAddress = returnFullAddress();
        console.log(myAddress);

        function printPlayerInfo() {
            console.log("Username : " + "dark_magician");
            console.log("Level : " + 95);
            console.log("Job : " + "Mage");

        }

        let user1 = printPlayerInfo();
        console.log(user1); //undefined

        function returnSumOf5and10() {
            return 5 + 10;
        }

        let sumOf5And10 = returnSumOf5and10(); //15
        console.log(sumOf5And10);

        let total = 100 + returnSumOf5and10(); //115
        console.log(total);

        function getGuildMembers() {
            return ['lulu', 'tristana', 'Teemo'];
        }

        console.log(getGuildMembers());

        //Function Naming Conventions

        function getCourses() {
            let courses = ['ReactJS 101', 'ExpressJS 101', 'MongoDB 101']
            return courses;
        }

        let courses = getCourses();
        console.log(courses);

        //Avoid using generic names and pointless and inappropriate function names

        //use camelCase

